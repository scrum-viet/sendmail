<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$key = $argv[1] ?? '*';

try {

    $emails = json_decode(file_get_contents('config/email.json'), true);
    if (!isset($emails[0])) {
        die('No emails');
    }
    $mailTemplates = include_once ('config/template.php');
    if (!isset($mailTemplates[$key])) {
        die('No template');
    }
    $mailTemplate = $mailTemplates[$key];
    $mailTemplate['body'] = include_once("config/templates/{$key}.php");
    $smtpConfig = include_once ('config/smtp.php');
    foreach ($emails as $info) {
        sendMail($info, $mailTemplate, $smtpConfig);
    }
    echo 'Message has been sent';
} catch (Exception $e) {
    var_dump($e->getMessage());
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}

function sendMail($info, $mailTemplate, $smtpConfig) {
    $mail = new PHPMailer(true);
    //Server settings
    $mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->CharSet = 'UTF-8';
    $mail->Host = $smtpConfig['host'];
    $mail->SMTPAuth = true;
    $mail->Username = $smtpConfig['username'];
    $mail->Password = $smtpConfig['password'];
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->Subject = $mailTemplate['subject'];
    $mail->setFrom($smtpConfig['from_email'], $smtpConfig['from_name']);
    $mail->addAddress($info['email'], $info['name']);
    $mail->isHTML(true);
    $mail->Body = str_replace('#NAME#', $info['name'], $mailTemplate['body']);
    $mail->send();
}