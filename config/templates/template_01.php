<?php
return <<<EOF
    <table border="0" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="jsConTent" style="width: 643px;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="font-size: 10pt; font-family: Verdana, Arial, Helvetica, sans-serif;">
<div style="text-align: center;">
<div><span style="font-family: arial, helvetica, sans-serif, sans-serif;"><img src="https://scontent.fsgn3-1.fna.fbcdn.net/v/t1.0-9/51691348_369692570521578_5114390813480058880_o.jpg?_nc_cat=111&amp;_nc_oc=AQliGj8coKSqtO-xWCHyhVK9XSYHmEkMF_zcc-xb875L_cXwiXta9u6G6o35d3FqYhQ&amp;_nc_ht=scontent.fsgn3-1.fna&amp;oh=3b08eb26866cd69081edafef20187b0a&amp;oe=5CF2FBD4" width="400" height="205" /></span></div>
</div>
<div><span style="background-color: #ffffff;"><span style="font-family: Arial;"><span style="font-size: 16px;"><span style="font-size: 16px;"><span style="color: #000000;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;"></span></span></span></span></span></span></div>
<div>&nbsp;</div>
<div><strong style="font-size: 16px; font-family: Arial; background-color: #ffffff; text-align: justify;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">#NAME#</span></strong> <span style="font-size: 16px; font-family: Arial;">ơi,</span></div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">Workshop </span><strong><span style="font-family: arial, helvetica, sans-serif, sans-serif;">"Học Scrum Đ&iacute;ch Thực Từ Game Giả Lập" </span></strong><span style="font-family: arial, helvetica, sans-serif, sans-serif;">- sự kiện về Scrum độc đ&aacute;o nhất đầu năm 2019 đ&atilde; ch&iacute;nh thức ch&agrave;o đ&oacute;n những lượt đăng k&yacute; đầu ti&ecirc;n. D&ugrave; bạn mới tập t&agrave;nh t&igrave;m hiểu Scrum hay đ&atilde; &aacute;p dụng framework n&agrave;y l&acirc;u năm, chỉ cần c&oacute; niềm đam m&ecirc; v&agrave; sẵn s&agrave;ng đ&oacute;n nhận những thử th&aacute;ch th&igrave; đ&acirc;y l&agrave; sự kiện bạn kh&ocirc;ng n&ecirc;n bỏ lỡ. </span></span></span></span></div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify; font-family: arial, helvetica, sans-serif, sans-serif; font-size: 16px;">- Thời gian: 9h00 - 12h00 thứ Bảy, ng&agrave;y 23/3/2019.</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify; font-family: arial, helvetica, sans-serif, sans-serif; font-size: 16px;">- Địa điểm: Marika Social Caf&eacute; - 74/13/4 Trương Quốc Dung, phường 10, Q. Ph&uacute; Nhuận.</div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify; font-family: arial, helvetica, sans-serif, sans-serif; font-size: 16px;">👉 Th&ocirc;ng tin chi tiết về sự kiện vui l&ograve;ng xem tại <a href="https://www.facebook.com/events/534886257001021/" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/events/534886257001021/&amp;source=gmail&amp;ust=1551534402354000&amp;usg=AFQjCNFyDxSADOFWpqD5ndupu9oo23-9fA">đ&acirc;y</a>.</div>
<div style="text-align: justify;">&nbsp;</div>
  <div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; line-height: 1.5;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">----------</span></span></span></span></div>
<div style="text-align: justify;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">H&atilde;y </span><strong><span style="font-family: arial, helvetica, sans-serif, sans-serif;">reply / trả lời</span></strong><span style="font-family: arial, helvetica, sans-serif, sans-serif;"> email n&agrave;y để x&aacute;c nhận với ch&uacute;ng t&ocirc;i rằng bạn chắc chắn sẽ c&oacute; mặt tại Workshop 02 với chủ đề v&ocirc; c&ugrave;ng th&uacute; vị n&agrave;y nh&eacute;.</span></span></span></span></div>
<div style="text-align: justify;">&nbsp;</div>
<div style="text-align: justify;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">Hẹn sớm gặp lại bạn. </span></span></span></span></div>
<div style="text-align: justify;">&nbsp;</div>
<p style="font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; margin: 0px 0px 8px; line-height: 1.5;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">Th&acirc;n &aacute;i,<br /></span><strong><span style="font-family: arial, helvetica, sans-serif, sans-serif;">Scrum Viet Team</span></strong></span></span></span></span></span></p>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; line-height: 1.5;">&nbsp;</div>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; text-align: center; line-height: 1.5;"><span style="background-color: #ffffff;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">----------</span></span></span></span></div>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; text-align: center; line-height: 1.5;">&nbsp;</div>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; line-height: 1.5; text-align: center;"><span style="color: #000000;"><span style="font-size: 16px;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;">Giữ li&ecirc;n lạc nh&eacute;!</span></span></span></div>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; line-height: 1.5; text-align: center;">&nbsp;</div>
<div style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13.3333px; font-style: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; line-height: 1.5; text-align: left;">
<div style="text-align: center; line-height: 1.5;"><span style="font-family: arial, helvetica, sans-serif, sans-serif;"><a href="http://scrumviet.com" target="_blank" rel="noopener"><img src="https://s3-ap-southeast-1.amazonaws.com/scrumviet/logo.png" width="115" height="33" /></a>&nbsp;&nbsp;<a href="mailto:support@scrumviet.com?subject=Scrum Viet" target="_blank" rel="noopener"><img src="https://s3-ap-southeast-1.amazonaws.com/scrumviet/email_01.png" height="34" /></a>&nbsp;&nbsp;<a href="https://www.facebook.com/Scrum-Viet-289357721888397/" target="_blank" rel="noopener"><img src="https://s3-ap-southeast-1.amazonaws.com/scrumviet/fb.png" width="34" height="34" /></a></span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</td>
</tr>
</tbody>
</table>
EOF;
