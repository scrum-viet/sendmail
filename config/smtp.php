<?php
return [
    'host' => 'smtp_host',
    'username' => 'username',
    'password' => 'password',
    'from_email' => 'from_email@yopmail.com',
    'from_name' => 'From Name'
];